(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.MobileButton = {
        toggleState: function(event) {
            event.preventDefault();

            var $handle = $(this),
                $nav    = $('#nav-mobile');

            $nav.slideToggle();
            $handle.toggleClass('close');
        },

        init: function() {
            $('#mobile-toggle').on('click touchstart', this.toggleState);
        }
    };

    App.Home = {
        bannersCycle: function() {
            $('#banners-cycle').cycle({
                slides: '>a',
                pagerTemplate: '<a href=#>{{slideNum}}</a>'
            });
        },

        init: function() {
            if ($('.main').hasClass('home')) {
                this.bannersCycle();
            }
        }
    };

    App.Noticias = {
        chamadasCycle: function() {
            $('#noticias-cycle').cycle({
                slides: '>a',
                pager: '.noticias-cycle-pager',
                pagerTemplate: '<a href="{{href}}">{{title}}<span>LER MAIS »</span></a>'
            });

            $('.noticias-cycle-pager.pager-aside a').on('click touchstart', function() {
                window.location.href = this.href;
            });
        },

        listaNoticiasAnos: function(event) {
            event.preventDefault();

            var $wrapper = $(this).parent(),
                $lista   = $(this).next(),
                $other   = $('.lista-wrapper').not($wrapper);

            if ($wrapper.hasClass('open')) return;

            $other.find('div').slideUp('slow', function () {
                $other.removeClass('open');
            });
            $lista.slideDown('slow', function () {
                $wrapper.addClass('open');
            });
        },

        init: function() {
            if ($('.main').hasClass('noticias')) {
                this.chamadasCycle();
                $('.lista-wrapper .trigger').on('click touchstart', this.listaNoticiasAnos);
            }
        }
    };

    App.Lightbox = {
        init: function() {
            $('.lightbox').fancybox({
                helpers: {
                    overlay: {
                        locked: false
                    },
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    };

    App.init = function() {
        this.MobileButton.init();
        this.Home.init();
        this.Noticias.init();
        this.Lightbox.init();
    };

    $(document).ready(function() {
        App.init();
    });

}(window, document, jQuery));
