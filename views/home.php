    <div class="main home">
        <div class="banners">
            <div class="center">
                <div id="banners-cycle">
                    <a href="LINK-DO-SLIDE1" class="banners-slide" style="background-image: url('<?=$url?>assets/img/imagem-banner.png');">
                        <div>
                            <p class="chamada">Conheça nossos serviços:</p>
                            <p class="titulo">Certificações de Produtos e Sistemas de Gestão</p>
                            <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tincidunt semper nunc, vitae vehicula dui gravida vitae.</p>
                        </div>
                    </a>
                    <a href="LINK-DO-SLIDE2" class="banners-slide" style="background-image: url('<?=$url?>assets/img/imagem-banner.png');">
                        <div>
                            <p class="chamada">Conheça nossos serviços:</p>
                            <p class="titulo">Lorem ipsum dolor sit amet, consectetur</p>
                            <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </a>

                    <div class="cycle-pager"></div>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="laboratorios">
                <h3>Saiba mais sobre nossos <span>Laboratórios</span>:</h3>
                <a href="#">Ensaios Mecânicos</a>
                <a href="#">Metalurgia</a>
                <a href="#">Polímeros</a>
                <a href="#">EPI</a>
                <a href="#">Calibração</a>
                <a href="#">Químico</a>
                <a href="#">Químico Industrial</a>
                <a href="#">Químico Metais</a>
                <a href="#">Químico Orgânico</a>
                <a href="#">Tintas e Revestimentos</a>
                <a href="#">Química Fina</a>
                <a href="#">Microbiologia</a>
                <a href="#">Químico Têxtil</a>
                <a href="#">Bens de Consumo</a>
                <a href="#">Elétrico</a>
            </div>

            <div class="noticias">
                <h3>Notícias</h3>

                <a href="#" class="noticia">
                    <p class="titulo">Título da Notícia em Destaque</p>
                    <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed gravida turpis. Maecenas velit dui, commodo sit amet varius id, ultrices nec nisl...</p>
                    <span>LER MAIS »</span>
                </a>
                <a href="#" class="noticia">
                    <p class="titulo">Título da Notícia em Destaque</p>
                    <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed gravida turpis. Maecenas velit dui, commodo sit amet varius id, ultrices nec nisl...</p>
                    <span>LER MAIS »</span>
                </a>
                <a href="#" class="noticia">
                    <p class="titulo">Título da Notícia em Destaque</p>
                    <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed gravida turpis. Maecenas velit dui, commodo sit amet varius id, ultrices nec nisl...</p>
                    <span>LER MAIS »</span>
                </a>
            </div>
        </div>
    </div>
