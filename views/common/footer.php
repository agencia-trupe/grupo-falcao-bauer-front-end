
    <footer>
        <div class="info">
            <div class="center">
                <div class="contato">
                    <div class="logo-footer"></div>
                    <p class="endereco">
                        Rua Aquinos, 111 · 3º Andar<br>
                        Água Branca · São Paulo/SP<br>
                        05036-070
                    </p>
                    <p class="telefone">(11) 3611·1729</p>
                </div>

                <div class="col">
                    <a href="#">» GRUPO FALCÃO BAUER</a>
                    <a href="#">» INSTITUTO FALCÃO BAUER</a>
                    <a href="#">» NOTÍCIAS</a>
                    <a href="#">» TRABALHE CONOSCO</a>
                    <a href="#">» CONTATO</a>
                </div>

                <div class="col">
                    <a href="#">» ENGENHARIA & TECNOLOGIA DA CONSTRUÇÃO</a>
                    <a href="#">» PETRÓLEO & GÁS</a>
                    <a href="#">» MEIO AMBIENTE</a>
                    <a href="#">» CERTIFICAÇÕES</a>
                    <a href="#">» LABORATÓRIOS</a>
                    <a href="#">» CONSULTORIA TÉCNICA</a>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <p>
                    © 2016 GRUPO FALCÃO BAUER · Todos os direitos reservados. |
                    <span>
                        <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                        <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa.</a>
                    </span>
                </p>
            </div>
        </div>
    </footer>
