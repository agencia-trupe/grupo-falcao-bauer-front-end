    <header>
        <div class="header-top">
            <div class="center">
                <h1><a href="<?=$url?>home">Grupo Falcão Bauer</a></h1>

                <form action="<?=$url?>busca" method="post" id="form-busca">
                    <input type="text" name="busca" placeholder="buscar no site" required>
                    <input type="submit" value>
                </form>

                <nav>
                    <a href="<?=$url?>grupo"<?php if($menu == 'grupo') echo 'class="active"' ?>>Grupo Falcão Bauer</a>
                    <a href="<?=$url?>cliente"<?php if($menu == 'cliente') echo 'class="active"' ?>>Área do Cliente</a>
                    <a href="<?=$url?>noticias"<?php if($menu == 'noticias') echo 'class="active"' ?>>Notícias</a>
                    <a href="<?=$url?>trabalhe-conosco"<?php if($menu == 'trabalhe-conosco') echo 'class="active"' ?>>Trabalhe Conosco</a>
                    <a href="<?=$url?>contato"<?php if($menu == 'contato') echo 'class="active"' ?>>Contato</a>
                </nav>
            </div>
        </div>

        <div class="header-nav">
            <div class="center">
                <nav>
                    <a href="<?=$url?>construcao"<?php if($menu == 'construcao') echo 'class="active"' ?>>Construção</a>
                    <a href="<?=$url?>petroleo-e-gas"<?php if($menu == 'petroleo-e-gas') echo 'class="active"' ?>>Petróleo & Gás</a>
                    <a href="<?=$url?>meio-ambiente"<?php if($menu == 'meio-ambiente') echo 'class="active"' ?>>Meio Ambiente</a>
                    <a href="<?=$url?>certificacoes"<?php if($menu == 'certificacoes') echo 'class="active"' ?>>Certificações</a>
                    <a href="<?=$url?>laboratorios"<?php if($menu == 'laboratorios') echo 'class="active"' ?>>Laboratórios</a>
                    <a href="<?=$url?>consultoria-tecnica"<?php if($menu == 'consultoria-tecnica') echo 'class="active"' ?>>Consultoria Técnica</a>
                </nav>
            </div>
        </div>

        <div id="nav-mobile">
            <nav>
                <a href="<?=$url?>grupo"<?php if($menu == 'grupo') echo 'class="active"' ?>>Grupo Falcão Bauer</a>
                <a href="<?=$url?>cliente"<?php if($menu == 'cliente') echo 'class="active"' ?>>Área do Cliente</a>
                <a href="<?=$url?>noticias"<?php if($menu == 'noticias') echo 'class="active"' ?>>Notícias</a>
                <a href="<?=$url?>trabalhe-conosco"<?php if($menu == 'trabalhe-conosco') echo 'class="active"' ?>>Trabalhe Conosco</a>
                <a href="<?=$url?>contato"<?php if($menu == 'contato') echo 'class="active"' ?>>Contato</a>
                <a href="<?=$url?>construcao"<?php if($menu == 'construcao') echo 'class="active"' ?>>Construção</a>
                <a href="<?=$url?>petroleo-e-gas"<?php if($menu == 'petroleo-e-gas') echo 'class="active"' ?>>Petróleo & Gás</a>
                <a href="<?=$url?>meio-ambiente"<?php if($menu == 'meio-ambiente') echo 'class="active"' ?>>Meio Ambiente</a>
                <a href="<?=$url?>certificacoes"<?php if($menu == 'certificacoes') echo 'class="active"' ?>>Certificações</a>
                <a href="<?=$url?>laboratorios"<?php if($menu == 'laboratorios') echo 'class="active"' ?>>Laboratórios</a>
                <a href="<?=$url?>consultoria-tecnica"<?php if($menu == 'consultoria-tecnica') echo 'class="active"' ?>>Consultoria Técnica</a>
            </nav>
        </div>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>

