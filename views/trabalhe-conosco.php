    <div class="main trabalhe-conosco">
        <div class="title">
            <div class="center">
                <h2>Trabalhe Conosco</h2>
            </div>
        </div>

        <div class="center">
            <div class="texto">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis autem quia nobis consequatur harum molestias amet veritatis quisquam maiores vitae culpa, atque similique iure vel obcaecati, accusamus fugit omnis repellendus aspernatur illo tempore! Alias consequatur, ut quaerat, ratione, dolorem maxime beatae perferendis aperiam sunt quia amet! Sed alias repellat, laborum.</p>

                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                </ul>

                <a href="#">Título para o link de direcionamento do serviço de recrutamento online</a>
            </div>

            <div class="imagem">
                <img src="<?=$url?>assets/img/layout/img-trabalheconosco.png" alt="">
            </div>
        </div>
    </div>