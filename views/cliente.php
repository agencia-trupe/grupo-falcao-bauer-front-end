    <div class="main cliente">
        <div class="title">
            <div class="center">
                <h2>Área do Cliente</h2>
            </div>
        </div>

        <div class="center">
            <div class="texto">
                <a href="#">Resultados de Ensaios</a>
                <a href="#">Outro sistema - Cadastrável com link externo</a>
            </div>

            <div class="imagem">
                <img src="<?=$url?>assets/img/layout/img-areacliente.png" alt="">
            </div>
        </div>
    </div>