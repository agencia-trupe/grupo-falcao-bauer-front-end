    <div class="main noticias">
        <div class="title">
            <div class="center">
                <h2>Notícias</h2>
            </div>
        </div>

        <div class="center">
            <div class="slider-wrapper">
                <div id="noticias-cycle">
                    <a href="<?=$url?>noticias/noticia-com-foto" class="noticias-slide" style="background-image: url('<?=$url?>assets/img/noticias-slide1.jpg')" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, tempora!">
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, tempora!</span>
                    </a>
                    <a href="<?=$url?>noticias/noticia-sem-foto" class="noticias-slide" style="background-image: url('<?=$url?>assets/img/noticias-slide2.jpg')" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, sequi!">
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, sequi!</span>
                    </a>
                    <a href="<?=$url?>noticias/noticia-sem-foto" class="noticias-slide" style="background-image: url('<?=$url?>assets/img/noticias-slide3.jpg')" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, ad.">
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, ad.</span>
                    </a>
                </div>
                <div class="noticias-cycle-pager pager-aside"></div>
                <div class="noticias-cycle-pager pager-bullets"></div>
            </div>

            <div class="noticias-assuntos">
                <p>Selecione por assunto:</p>
                <a href="#">Sustentabilidade</a>
                <a href="#">Construção Civil</a>
                <a href="#">Certificação de Sistema de Gestão</a>
                <a href="#">Produto</a>
                <a href="#">Serviços</a>
                <a href="#">Treinamentos</a>
                <a href="#">Certificações no Brasil</a>
            </div>

            <div class="noticias-lista-index">
                <div class="lista-wrapper open">
                    <a href="#" class="trigger">2015</a>
                    <div>
                        <a href="<?=$url?>noticias/noticia-com-foto">
                            <span>12 maio</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                        <a href="<?=$url?>noticias/noticia-com-foto">
                            <span>22 dezembro</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                        <a href="<?=$url?>noticias/noticia-sem-foto">
                            <span>12 maio</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                    </div>
                </div>
                <div class="lista-wrapper">
                    <a href="#" class="trigger">2014</a>
                    <div>
                        <a href="<?=$url?>noticias/noticia-sem-foto">
                            <span>12 maio</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                        <a href="<?=$url?>noticias/noticia-sem-foto">
                            <span>22 dezembro</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                        <a href="<?=$url?>noticias/noticia-sem-foto">
                            <span>12 maio</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
