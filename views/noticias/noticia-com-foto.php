    <div class="main noticias">
        <div class="title">
            <div class="center">
                <h2>Notícias</h2>
            </div>
        </div>

        <div class="center">
            <div class="noticias-texto">
                <h3>NOVA PORTARIA 5599/63 PARA A PRODUÇÃO DE BRINQUEDOS É APROVADA</h3>
                <div class="texto">
                    <img src="<?=$url?>assets/img/noticias-post.jpg" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut soluta laborum minus. Iure, necessitatibus, provident repellendus ipsum, nostrum odio ipsam sint dolor illo praesentium voluptates!</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dola aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit.</p>
                    <p>Lorem ipsum dolor sit amet, consecte erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilis accumsan et iusto odio dignissim qui blandit.</p>
                </div>

                <div class="fonte">
                    <span class="subtitulo">Consulte a fonte da informação aqui:</span>
                    <a href="http://www.inpi.org.br/inep?=2847?linea=true" target="_blank">www.inpi.org.br/inep?=2847?linea=true</a>
                </div>

                <div class="documentos">
                    <span class="subtitulo">Documentos para download:</span>
                    <a href="#">Nome do documento para download</a>
                    <a href="#">Nome do documento para download</a>
                </div>
            </div>

            <div class="noticias-aside">
                <div class="destaque">
                    <a href="#">Sapiente animi, dolorum pariatur nemo. Doloribus dignissimos at laborum possimus aliquam nam.</a>
                    <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates deserunt molestias corrupti placeat voluptatem?</a>
                    <a href="#">Tenetur voluptatum velit vero natus laborum esse eius, nam!</a>
                </div>

                <div class="lista">
                    <span>2015</span>
                    <div>
                        <a href="#">
                            <span>12 maio</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                        <a href="#">
                            <span>22 dezembro</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                        <a href="#">
                            <span>12 maio</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint maiores quis adipisci voluptatem mollitia iure vitae itaque cumque ipsum quaerat.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
