    <div class="main grupo">
        <div class="title">
            <div class="center">
                <h2>Grupo Falcão Bauer</h2>
            </div>
        </div>

        <div class="center">
            <div class="texto">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste mollitia necessitatibus autem similique. Quos natus doloribus aliquam cum sapiente reprehenderit nemo eveniet voluptatibus deserunt at tenetur, dolor velit officiis quis ex aperiam cumque ducimus quam itaque temporibus. Architecto debitis, eius ipsam sapiente omnis eligendi rerum! Assumenda amet fuga fugiat, doloribus nam, eius eaque non sint suscipit natus doloremque laboriosam. Quo vero voluptatibus facilis placeat aut vitae saepe, nulla quae exercitationem quia ut, et nam neque fugiat aliquam ad porro provident expedita fuga, iste odio quasi possimus consequatur, dolores! Sint modi a velit magnam dolorem nam corporis porro, tempore delectus aperiam?</p>
                <ul>
                    <li>» CONSULTORIA - BNA</li>
                    <li>» ENGENHARIA E CONSTRUÇÃO</li>
                    <li>» ENSAIOS MECÂNICO E METALOGRÁFICO</li>
                    <li>» LABORATÓRIO QUÍMICO</li>
                    <li>» MEIO AMBIENTE</li>
                    <li>» PETRÓLEO E PETROQUÍMICO</li>
                </ul>
            </div>

            <div class="imagem">
                <img src="<?=$url?>assets/img/layout/img-grupofalcaobauer.png" alt="">
            </div>

            <div class="texto-row">
                <div class="texto-col">
                    <h3>Equipe Técnica</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, animi autem ducimus blanditiis, quasi debitis odio corrupti deserunt quaerat natus nulla ut iste ad expedita ullam et! Qui, quo adipisci.</p>
                </div>

                <div class="texto-col">
                    <h3>Confidencialidade e Direito de Propriedade do Cliente:</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam optio illo maiores vel iste provident distinctio, debitis est molestiae, totam ea aut harum, ipsum dolore! Provident libero labore unde? Deserunt natus, nostrum, amet voluptatibus earum accusantium corrupti iste, tempora nisi soluta maiores, voluptatem excepturi. Commodi ratione quia autem non minima.</p>
                </div>
            </div>

            <div class="texto-row">
                <h2>Política Integrada</h2>
                <div class="texto-col">
                    <h3>Qualidade, Meio Ambiente, Segurança e Saúde Ocupacional</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque perspiciatis eveniet impedit, esse rerum numquam ducimus fugiat accusamus, aut, quis in perferendis quae. Nobis eaque obcaecati ipsam, qui quas possimus.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum dignissimos architecto, aspernatur corporis quibusdam laborum quo tenetur magnam facilis totam. Dolore nobis non cum laboriosam similique nam voluptatibus doloribus culpa!</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, iste.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, fugit?</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, placeat!</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, sapiente.</li>
                    </ul>
                </div>

                <div class="texto-col">
                    <h3>Missão</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In nam, earum officia quaerat voluptatem hic animi tenetur dolores fugit molestiae eveniet quae provident culpa esse, consequatur suscipit iure. Repellat, fugit.</p>

                    <h3>Visão</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae repellat totam cumque numquam amet harum magnam eveniet at veniam molestiae.</p>

                    <h3>Acreditações</h3>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, iste.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, fugit?</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, placeat!</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, sapiente.</li>
                    </ul>
                </div>
            </div>

            <div class="texto-row">
                <div class="texto-col">
                    <h3>Certificações</h3>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, iste.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, fugit?</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, placeat!</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, sapiente.</li>
                    </ul>
                </div>

                <div class="texto-col">
                    <h3>Qualificações e Associações</h3>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, iste.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, fugit?</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, placeat!</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, sapiente.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
