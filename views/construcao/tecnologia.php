    <div class="main construcao">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>Engenharia & Tecnologia da Construção</h2>

                <nav>
                    <a href="#">PNEUS</a>
                    <a href="<?=$url?>construcao/tecnologia" class="active">TECNOLOGIA</a>
                    <div class="submenu">
                        <a href="<?=$url?>construcao/tecnologia-concreto">Concreto, cimentos, agregados e aditivos para concreto</a>
                        <a href="#">Consultoria técnica de estruturas</a>
                        <a href="#">Monitoramento estrutural</a>
                    </div>
                    <a href="<?=$url?>construcao/laboratorios">LABORATÓRIOS</a>
                </nav>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-construcao.png" alt="">

                <h3>Tecnologia</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod consequuntur delectus quam exercitationem quo placeat, alias, atque nostrum accusantium quaerat voluptas a recusandae minima totam ullam saepe dolores fugiat fugit.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, incidunt.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente voluptates ut molestias, est aspernatur repellendus aperiam dolorum maiores at temporibus non eaque sunt, eligendi. Maxime nulla minima mollitia beatae velit, voluptates placeat, fuga, maiores eum quas molestiae natus impedit modi.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque blanditiis ab aliquid tempora. Rerum doloremque dolor, minima ipsa tenetur nemo.</p>
            </div>

        </div>
    </div>
