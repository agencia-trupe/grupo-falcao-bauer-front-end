    <div class="main construcao">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>Engenharia & Tecnologia da Construção</h2>

                <nav>
                    <a href="#">PNEUS</a>
                    <a href="<?=$url?>construcao/tecnologia">TECNOLOGIA</a>
                    <a href="<?=$url?>construcao/laboratorios" class="active">LABORATÓRIOS</a>
                    <div class="submenu">
                        <a href="#" class="active">Laboratórios de produtos da construção civil</a>
                    </div>
                </nav>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-construcao.png" alt="">

                <h3>Laboratórios</h3>
                <h3>Laboratório de produtos da construção civil</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod consequuntur delectus quam exercitationem quo placeat, alias, atque nostrum accusantium quaerat voluptas a recusandae minima totam ullam saepe dolores fugiat fugit.</p>

                <p class="detalhes">
                    Saiba mais detalhes:
                    <a href="#">Laboratório de produtos da construção civil</a>
                </p>
            </div>

        </div>
    </div>
