    <div class="main construcao">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>Engenharia & Tecnologia da Construção</h2>

                <nav>
                    <a href="#">PNEUS</a>
                    <a href="<?=$url?>construcao/tecnologia" class="active">TECNOLOGIA</a>
                    <div class="submenu">
                        <a href="<?=$url?>construcao/tecnologia-concreto" class="active">Concreto, cimentos, agregados e aditivos para concreto</a>
                        <a href="#">Consultoria técnica de estruturas</a>
                        <a href="#">Monitoramento estrutural</a>
                    </div>
                    <a href="<?=$url?>construcao/laboratorios">LABORATÓRIOS</a>
                </nav>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-construcao.png" alt="">

                <h3>Tecnologia</h3>
                <h3>Concreto, cimentos, agregados e aditivos para concreto</h3>

                <h4>Estudos de Dosagem</h4>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, sequi.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias omnis repellat, ab ea incidunt ratione.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, repellat.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis temporibus alias fugiat doloremque dolor veritatis dignissimos, optio minima saepe rerum.</li>
                    <li>Lorem ipsum dolor sit amet.</li>
                </ul>

                <h4>Caracterização das propriedades do concreto no estado fresco</h4>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, sequi.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias omnis repellat, ab ea incidunt ratione.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, repellat.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis temporibus alias fugiat doloremque dolor veritatis dignissimos, optio minima saepe rerum.</li>
                    <li>Lorem ipsum dolor sit amet.</li>
                </ul>

                <h4>Caracterização das propriedades do concreto no estado endurecido</h4>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, sequi.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias omnis repellat, ab ea incidunt ratione.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, repellat.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis temporibus alias fugiat doloremque dolor veritatis dignissimos, optio minima saepe rerum.</li>
                    <li>Lorem ipsum dolor sit amet.</li>
                </ul>

                <div class="imagens">
                    <a class="lightbox" rel="galeria" title="Determinação da resistência à compressão" href="<?=$url?>assets/img/imagem-lightbox.jpg">
                        <img src="<?=$url?>assets/img/imagem-lightbox.jpg" alt="">
                        <span>Determinação da resistência à compressão</span>
                    </a>

                    <a class="lightbox" rel="galeria" title="Determinação da resistência à compressão" href="<?=$url?>assets/img/imagem-lightbox.jpg">
                        <img src="<?=$url?>assets/img/imagem-lightbox.jpg" alt="">
                        <span>Determinação da resistência à compressão</span>
                    </a>

                    <a class="lightbox" rel="galeria" title="Determinação da resistência à compressão" href="<?=$url?>assets/img/imagem-lightbox.jpg">
                        <img src="<?=$url?>assets/img/imagem-lightbox.jpg" alt="">
                        <span>Determinação da resistência à compressão</span>
                    </a>

                    <a class="lightbox" rel="galeria" title="Determinação da resistência à compressão" href="<?=$url?>assets/img/imagem-lightbox.jpg">
                        <img src="<?=$url?>assets/img/imagem-lightbox.jpg" alt="">
                        <span>Determinação da resistência à compressão</span>
                    </a>

                    <a class="lightbox" rel="galeria" title="Determinação da resistência à compressão" href="<?=$url?>assets/img/imagem-lightbox.jpg">
                        <img src="<?=$url?>assets/img/imagem-lightbox.jpg" alt="">
                        <span>Determinação da resistência à compressão</span>
                    </a>
                </div>
            </div>

        </div>
    </div>
