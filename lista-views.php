<?php

$url = 'http://grupofalcaobauer.dev/';

foreach (scandir('views/') as $file) {
    if ($file != '.' && $file != '..' && $file != 'common') {
        if (is_dir('views/'.$file)) {
            echo '<h1><a href="'.$url.$file.'">'.$file.'</a></h1><ul>';
            foreach (scandir('views/'.$file.'/') as $subfile) {
                if ($subfile != '.' && $subfile != '..' && $subfile != 'index.php' && !is_dir('views/'.$file.'/'.$subfile)) {
                    echo '<li><a href="'.$file.'/'.str_replace('.php', '', $subfile).'">'.str_replace('.php', '', $subfile).'</a></li>';
                }
            }
            echo '</ul>';
        } else {
            echo '<h1><a href="'.$url.str_replace('.php', '', $file).'">'.str_replace('.php', '', $file).'</a></h1>';
        }
        echo '<hr>';
    }
}
echo '<h1><a href="'.$url.'404">erro 404</a></h1>';

?>
<style>
    body {
        font-family: 'Courier New';
        font-size: 14px;
        width: 600px;
        margin: 0 auto;
        padding: 60px 0;
    }
    h1 { font-size: 18px; }
    hr { margin: 30px 0; height: 1px; border: 0; background: #9B9B9B; }
    a { text-decoration: none; }
</style>
